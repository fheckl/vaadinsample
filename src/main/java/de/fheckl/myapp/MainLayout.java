package de.fheckl.myapp;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;

@Route("")
@PWA(name = "Project Base for Vaadin", shortName = "Project Base")
public class MainLayout extends VerticalLayout implements RouterLayout {

    public MainLayout() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.add(new Button("Issue 7143", event -> UI.getCurrent().navigate(Problem1.class)));
        buttonBar.add(new Button("Issue 7152", event -> UI.getCurrent().navigate(Problem2.class)));
        add(buttonBar);
    }
}

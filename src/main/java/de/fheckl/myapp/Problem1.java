package de.fheckl.myapp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Route(value = "problem1", layout = MainLayout.class)
public class Problem1 extends VerticalLayout {

    private VerticalLayout container = new VerticalLayout();
    private VerticalLayout layout1 = new VerticalLayout();
    private VerticalLayout layout2 = new VerticalLayout();

    public Problem1() {
        setupIntroduction();
        setupActionLayout();
    }

    private void setupActionLayout() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.add(new Button("Toggle layouts", event -> toggleLayouts()));
        add(buttonBar);

        setupSwitchableLayouts();
        container.add(layout1);

        add(container);
    }

    private void setupIntroduction() {
        add(new H2("Demonstrating Issue 7143"));
        final Anchor anchor = new Anchor("https://github.com/vaadin/flow/issues/7143");
        anchor.setText("see github.com");
        add(anchor);
        add(new Paragraph("When you click the 'Toggle layouts' button below it will switch between two layouts, leading to a crash when switching the original layout back in."));
    }

    private void toggleLayouts() {
        Component layoutToShow = layout1.getElement().getNode().isAttached() ? layout2 : layout1;

        container.removeAll();
        container.add(layoutToShow);
    }

    private void setupSwitchableLayouts() {
        setupLayout1();
        setupLayout2();
    }

    private void setupLayout1() {
        layout1.add(new Label("This is layout 1"));

        // create some objects
        SimplePojo obj1 = new SimplePojo("Franz", 17);
        SimplePojo obj2 = new SimplePojo("Franzi", 16);

        // manually create the component to display for each object
        Map<SimplePojo, Button> buttonMap = new HashMap<>();
        buttonMap.put(obj1, new Button("obj1"));
        buttonMap.put(obj2, new Button("obj2"));

        // create the grid
        Grid<SimplePojo> grid = new Grid<>(SimplePojo.class);
        grid.addColumn(new ComponentRenderer<>(buttonMap::get));
        grid.setItems(Arrays.asList(obj1, obj2));
        grid.setHeightByRows(true);
        layout1.add(grid);
    }

    private void setupLayout2() {
        layout2.add(new Label("This is layout 2"));
    }
}

package de.fheckl.myapp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;

import java.util.Collections;

@Route(value = "problem2", layout = MainLayout.class)
public class Problem2 extends VerticalLayout {

    public Problem2() {
        setupIntroduction();
        add(createLayout());
    }

    private void setupIntroduction() {
        add(new H2("Demonstrating Issue 7152"));
        final Anchor anchor = new Anchor("https://github.com/vaadin/flow/issues/7152");
        anchor.setText("see github.com");
        add(anchor);
        add(new Paragraph("When you click the button in the table, new content will appear below the table. The button doesn't work thereafter (see terminal console)."));
    }

    private Component createLayout() {
        final VerticalLayout panel = new VerticalLayout();

        final Paragraph textBelow = new Paragraph("When you click the button again, 'Got an RPC for non-existent node' will be logged!");
        final Grid<SimplePojo> grid = new Grid<>(SimplePojo.class);

        Button button = new Button("I will only work once");
        button.addClickListener(clickEvent -> {
            panel.removeAll();
            panel.add(grid, textBelow);
        });

        grid.addColumn(new ComponentRenderer<>(pojo -> button));
        grid.setItems(Collections.singletonList(new SimplePojo("Dan", 55)));
        grid.setHeightByRows(true);

        panel.add(grid);

        return panel;
    }

}
